image:
  name: hashicorp/terraform:0.12.21
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

stages:
  - test_and_lint
  - build_and_push
  - staging_plan
  - staging_apply
  - production_plan
  - production_apply
  - destroy

test_and_lint:
  image: docker:19.03.5
  rules:
    - if: "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(master|production)$/ || $CI_COMMIT_BRANCH =~ /^(master|production)$/"
  script:
    - apk add python3-dev libffi-dev openssl-dev gcc libc-dev make
    - pip3 install docker-compose
    - docker-compose run --rm app sh -c "python manage.py wait_for_db && python manage.py test && flake8"
  services:
    - docker:19.03.5-dind
  stage: test_and_lint

validate_terraform:
  rules:
    - if: "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(master|production)$/ || $CI_COMMIT_BRANCH =~ /^(master|production)$/"
  script:
    - cd deploy
    - terraform init -backend=false
    - terraform validate
    - terraform fmt -check
  stage: test_and_lint

build_and_push:
  image: docker:19.03.5
  rules:
    - if: "$CI_COMMIT_BRANCH =~ /^(master|production)$/"
  script:
    - apk add python3
    - pip3 install awscli
    - docker build --compress -t $ECR_REPO:$CI_COMMIT_SHORT_SHA .
    - $(aws ecr get-login --no-include-email --region us-east-1)
    - docker push $ECR_REPO:$CI_COMMIT_SHORT_SHA
    - docker tag $ECR_REPO:$CI_COMMIT_SHORT_SHA $ECR_REPO:latest
    - docker push $ECR_REPO:latest
  services:
    - docker:19.03.5-dind
  stage: build_and_push

staging_plan:
  rules:
    - if: "$CI_COMMIT_BRANCH =~ /^(master|production)$/"
  script:
    - cd deploy/
    - export TF_VAR_ecr_image_api=$ECR_REPO:$CI_COMMIT_SHORT_SHA
    - terraform init
    - terraform workspace select staging || terraform workspace new staging
    - terraform plan
  stage: staging_plan

staging_apply:
  rules:
    - if: "$CI_COMMIT_BRANCH =~ /^(master|production)$/"
  script:
    - cd deploy/
    - export TF_VAR_ecr_image_api=$ECR_REPO:$CI_COMMIT_SHORT_SHA
    - terraform init
    - terraform workspace select staging
    - terraform apply -auto-approve
  stage: staging_apply

production_plan:
  rules:
    - if: "$CI_COMMIT_BRANCH == 'production'"
  script:
    - cd deploy/
    - export TF_VAR_ecr_image_api=$ECR_REPO:$CI_COMMIT_SHORT_SHA
    - terraform init
    - terraform workspace select production || terraform workspace new production
    - terraform plan
  stage: production_plan

production_apply:
  rules:
    - if: "$CI_COMMIT_BRANCH == 'production'"
  script:
    - cd deploy/
    - export TF_VAR_ecr_image_api=$ECR_REPO:$CI_COMMIT_SHORT_SHA
    - terraform init
    - terraform workspace select production
    - terraform apply -auto-approve
  stage: production_apply

staging_destroy:
  rules:
    - if: "$CI_COMMIT_BRANCH =~ /^(master|production)$/"
      when: manual
  script:
    - cd deploy/
    - terraform init
    - terraform workspace select staging
    - terraform destroy -auto-approve
  stage: destroy

production_destroy:
  rules:
    - if: "$CI_COMMIT_BRANCH == 'production'"
      when: manual
  script:
    - cd deploy/
    - terraform init
    - terraform workspace select production
    - terraform destroy -auto-approve
  stage: destroy