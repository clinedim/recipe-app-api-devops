resource "aws_ecs_cluster" "main" {
  name = "${local.prefix}-cluster"
  tags = local.common_tags
}

resource "aws_iam_policy" "task_execution_role_policy" {
  description = "Allow retrieving of images and adding to logs"
  name        = "${local.prefix}-task-exec-role-policy"
  path        = "/"
  policy      = file("./templates/ecs/task-exec-role.json")
}

resource "aws_iam_role" "task_execution_role" {
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")
  name               = "${local.prefix}-task-exec-role"
}

resource "aws_iam_role_policy_attachment" "task_execution_role" {
  policy_arn = aws_iam_policy.task_execution_role_policy.arn
  role       = aws_iam_role.task_execution_role.name
}

resource "aws_iam_role" "app_iam_role" {
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")
  name               = "${local.prefix}-api-task"
  tags               = local.common_tags
}

resource "aws_cloudwatch_log_group" "ecs_task_logs" {
  name = "${local.prefix}-api"
  tags = local.common_tags
}

data "template_file" "api_container_definitions" {
  template = file("./templates/ecs/container-definitions.json.tpl")
  vars = {
    allowed_hosts     = "*"
    app_image         = var.ecr_image_api
    db_host           = aws_db_instance.main.address
    db_name           = aws_db_instance.main.name
    db_pass           = aws_db_instance.main.password
    db_user           = aws_db_instance.main.username
    django_secret_key = var.django_secret_key
    log_group_name    = aws_cloudwatch_log_group.ecs_task_logs.name
    log_group_region  = data.aws_region.current.name
    proxy_image       = var.ecr_image_proxy
  }
}

resource "aws_ecs_task_definition" "api" {
  container_definitions    = data.template_file.api_container_definitions.rendered
  cpu                      = 256
  execution_role_arn       = aws_iam_role.task_execution_role.arn
  family                   = "${local.prefix}-api"
  memory                   = 512
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  tags                     = local.common_tags
  task_role_arn            = aws_iam_role.app_iam_role.arn
  volume {
    name = "static"
  }
}

resource "aws_security_group" "ecs_service" {
  description = "Access for the ECS Service"
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 443
    protocol    = "tcp"
    to_port     = 443
  }
  egress {
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block
    ]
    from_port = 5432
    protocol  = "tcp"
    to_port   = 5432
  }
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 8000
    protocol    = "tcp"
    to_port     = 8000
  }
  name   = "${local.prefix}-ecs-service"
  tags   = local.common_tags
  vpc_id = aws_vpc.main.id
}

resource "aws_ecs_service" "api" {
  cluster     = aws_ecs_cluster.main.name
  launch_type = "FARGATE"
  name        = "${local.prefix}-api"
  network_configuration {
    assign_public_ip = true
    security_groups  = [aws_security_group.ecs_service.id]
    subnets = [
      aws_subnet.public_a.id,
      aws_subnet.public_b.id
    ]
  }
  task_definition = aws_ecs_task_definition.api.family
}