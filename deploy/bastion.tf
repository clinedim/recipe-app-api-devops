data "aws_ami" "amazon_linux" {
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
  most_recent = true
  owners      = ["amazon"]
}

resource "aws_iam_role" "bastion" {
  assume_role_policy = file("./templates/bastion/instance-profile-policy.json")
  name               = "${local.prefix}-bastion"
  tags               = local.common_tags
}

resource "aws_iam_role_policy_attachment" "bastion_attach_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.bastion.name
}

resource "aws_iam_instance_profile" "bastion" {
  name = "${local.prefix}-bastion-instance-profile"
  role = aws_iam_role.bastion.name
}

resource "aws_instance" "bastion" {
  ami                  = data.aws_ami.amazon_linux.id
  iam_instance_profile = aws_iam_instance_profile.bastion.name
  instance_type        = "t2.micro"
  key_name             = var.bastion_key_name
  subnet_id            = aws_subnet.public_a.id
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
  user_data = file("./templates/bastion/user-data.sh")
  vpc_security_group_ids = [
    aws_security_group.bastion.id
  ]
}

resource "aws_security_group" "bastion" {
  description = "Control bastion inbound and outbound access"
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
  }
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 443
    protocol    = "tcp"
    to_port     = 443
  }
  egress {
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block
    ]
    from_port = 5432
    protocol  = "tcp"
    to_port   = 5432
  }
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
  }
  name   = "${local.prefix}-bastion"
  tags   = local.common_tags
  vpc_id = aws_vpc.main.id
}