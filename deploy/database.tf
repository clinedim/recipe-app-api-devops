resource "aws_db_subnet_group" "main" {
  name = "${local.prefix}-main"
  subnet_ids = [
    aws_subnet.private_a.id,
    aws_subnet.private_b.id
  ]
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

resource "aws_security_group" "rds" {
  description = "Allow access to the RDS database instance"
  ingress {
    from_port = 5432
    protocol  = "tcp"
    security_groups = [
      aws_security_group.bastion.id,
      aws_security_group.ecs_service.id
    ]
    to_port = 5432
  }
  name   = "${local.prefix}-rds-inbound-access"
  tags   = local.common_tags
  vpc_id = aws_vpc.main.id
}

resource "aws_db_instance" "main" {
  allocated_storage       = 20
  backup_retention_period = 0
  db_subnet_group_name    = aws_db_subnet_group.main.name
  engine                  = "postgres"
  engine_version          = "11.4"
  identifier              = "${local.prefix}-db"
  instance_class          = "db.t2.micro"
  multi_az                = false
  name                    = "recipe"
  password                = var.db_password
  skip_final_snapshot     = true
  storage_type            = "gp2"
  username                = var.db_username
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
  vpc_security_group_ids = [aws_security_group.rds.id]
}