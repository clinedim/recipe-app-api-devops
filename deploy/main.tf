terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-tfstate-clinedim"
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
    encrypt        = true
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  common_tags = {
    Environment = terraform.workspace
    ManagedBy   = "Terraform"
    Owner       = var.contact
    Project     = var.project
  }
  prefix = "${var.prefix}-${terraform.workspace}"
}

data "aws_region" "current" {}