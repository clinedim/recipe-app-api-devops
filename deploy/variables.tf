variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "contact" {
  default = "marc.clinedinst@gmail.com"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "ecr_image_api" {
  default     = "018296510367.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
  description = "ECR image for API"
}

variable "ecr_image_proxy" {
  default     = "018296510367.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
  description = "ECR image for proxy"
}

variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

